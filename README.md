# Red Team Tech Notes

At GitLab one of our six [core values](https://about.gitlab.com/handbook/values/) is transparency. As a [Red Team](https://about.gitlab.com/handbook/engineering/security/red-team/) we are striving to uphold this value by sharing as much as we can. We named this project "Tech Notes" because the original idea was that we would share the technical things that we ourselves had to learn in our day to day work. We have expanded on that and will also use this project to publish results of Red Team exercises, technical papers, tooling, and blog posts that we write. We hope that by sharing we are not only demonstrating a company value but are also providing a resource for others to learn from. Of course, there will be scenarios where we are unable to share our work, the security of our customers and employees is why we are here so we will not share anything that creates a risk to either. Once there is no longer a risk, we will revisit publishing. If you have any questions or feedback on any of our content here please feel free to open and issue.

## Index

### Papers

* [Introducing Token Hunter](https://about.gitlab.com/blog/2019/12/20/introducing-token-hunter)
* [GCP Post Exploitation Tactics & Techniques](./gcp-post-exploitation-feb-2020/README.md)
* [The Mechanics of Modern Thievery (Part 1 of 2)](./mechanics-of-modern-thievery/part-1.md)
* [Privilege Escalation in Google Cloud Platform's OS Login](/oslogin-privesc-june-2020/)
* [K8s-GKE Attack Notes](/K8s-GKE-attack-notes/)
* [Firefox for Android LAN-Based Intent Triggering](/firefox-android-2020/)

### Quick Tips

* [Scraping Cloudflare Anti-Bot](/cloudflare-notes)

### Talks

* [Black Hat USA 2020 - Token-Hunter and gitrob:  Hunting for Secrets](./blackhat2020-hunting-for-secrets/)
* [BSides Melbourne 2020 - Google Cloud Post Exploitation Tactics & Techniques](https://www.youtube.com/watch?v=OJ_wUcVrGx0)

### Tools

* [GitRob](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/gitrob)
* [Token Hunter](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/token-hunter)
* [gcp_misc](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/gcp_misc)
* [gcp_enum](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/gcp_enum)
* [gcp_firewall_enum](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/gcp_firewall_enum)
* [gcp_k8s_enum](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/gcp_k8s_enum)

### Red Team Exercises

* [RT-011 Phishing Campaign - Fake Laptop Upgrade](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/red-team-tech-notes/-/tree/master/RT-011%20-%20Phishing%20Campaign)